//
//  ContentView.swift
//  TestBot
//
//  Created by Toshihiro Goto on 2020/01/17.
//  Copyright © 2020 Toshihiro Goto. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack{
            Text("Hello, World!!!!")
            Text("Hello, World!!!!")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
